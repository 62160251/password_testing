const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be  true ', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be false', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })
  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has  alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has  alphabet Z in password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})
describe('Test Digit ', () => {
  test('should has Digit 0-9 in password', () => {
    expect(checkDigit('0123456789')).toBe(true)
  })
  test('should has not Digit  in password', () => {
    expect(checkDigit('abcdefghij')).toBe(false)
  })
  test('should has  Digit  in password', () => {
    expect(checkDigit('9876543210')).toBe(true)
  })
})

describe('Test Symbol', () => {
  test('should has  Symbols  in password to be true', () => {
    expect(checkSymbol('45@46')).toBe(true)
  })

  test('should has not Symbols  in password', () => {
    expect(checkSymbol('abcdefghijklmnopqrstuvwxyz')).toBe(false)
  })

  test('should has not  Symbols  in password', () => {
    expect(checkSymbol('123465789')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password - A minimum of 8 characters (maximum 25)- Contain at least 1 uppercase or lowercase letter (A-Z, a-z)- Contain at least 1 number (0-9)- Contain at least 1 symbol (! " # $ % & ( ) * + , - . / : ; < = > ? @ [ ] ^ _ ` { | } ~) to be true', () => {
    expect(checkPassword('6216251@Ta')).toBe(true)
  })
  test('should password 62160251@Ta to be false', () => {
    expect(checkPassword('6216251Ta')).toBe(false)
  })
})
